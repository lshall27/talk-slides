# Talk Slides

## Description

A public repository of talks I have given, for if anyone wishes to make use of the slides (and for future Lynsey!). Talks are provided in pdf and pptx format. Although you will not be able to view the pptx format in this browser, if you click 

## Directory structure

Initially, each talk will be its own directory, with accompanying text file to describe the purpose of the talk. As I gather more talk slides over time, I will group these into topic directories, with talk subdirectories.

## Support
If you have any questions about the slides found in this directly, feel free to get in touch with me at HallL10@cardiff.ac.uk
