## Aim
The aim of this presentation was to explain the [departmental research software policy](https://wiki.psycm.cf.ac.uk/wiki/index.php/DPMCN_research_software_policy) to the psychosis group, and expand on the [software best practices](https://wiki.psycm.cf.ac.uk/wiki/index.php/Research_software_best_practices) that are made reference to in the policy. 
---
## Structure
### Desired topics covered included:
- promotion of the DPMCN research software policy
    - a simplified summary of what it constitutes
    - why it is important to divisional strategy
- obligation from researchers required to adhere to the policy
    - research software best practices
    - basic files requirements for repositories complying with the policy
    - determining an appropriate license
- the benefits of using the [Code Bank](https://wiki.psycm.cf.ac.uk/wiki/index.php/The_Codebank) for software development, beyond policy compliance
    - skills acquisition/ employability
    - visibility of code
    - code review (who and how)
- available training
    - git workshop
---
### Topics actually covered successfully:
- [X] promotion of the DPMCN research software policy
    - [X] a simplified summary of what it constitutes
    - [X] why it is important to divisional strategy
- [ ] obligation from researchers required to adhere to the policy
    - [X] research software best practices
    - [X] basic files requirements for repositories complying with the policy
    - [ ] determining an appropriate license
- [ ] the benefits of using the Code Bank for software development, beyond policy compliance
    - [X] skills acquisition/ employability
    - [ ] visibility of code
    - [ ] code review (who and how)
- [X] available training
    - [X] git workshop
---
## Audience feedback
Unsurprisingly, most questions asked were regarding areas that I didn't successfully cover in the talk. In particular:
- determining an appropriate license
- what a citation file is, and how to write one
- the implications of copyright being reserved to Cardiff University, particularly
    - how researchers could ensure access to their code after their contract ended
- why the Code Bank ([hosted on gitlab](https://git.cardiff.ac.uk/dpmcn-codebank)), and not just an open GitHub repository
- who can interact with code on the Code Bank
---
## Next steps...
Whilst questions were answered in person, it would be useful to create additional resources and amend existing ones in light of the user feedback. I have proposed:
- [ ] clarifying the text on the [Research Software Policy wiki page](https://wiki.psycm.cf.ac.uk/wiki/index.php/DPMCN_research_software_policy) to make it more user-friendly
- [ ] expanding the text on the [Code Bank wiki page](https://wiki.psycm.cf.ac.uk/wiki/index.php/The_Codebank) to include more information on the practicalities of using the Code Bank, such as:
    - [ ] rationale for GitLab vs GitHub
    - [ ] different roles and what they mean (e.g. reporter, developer, maintainer, owner)
    - [ ] the slack update channel
    - [ ] code review, who and how to
    - [ ] how to mirror to a public GitHub repository
- [ ] creating a resource of the "need to know" points of licensing in simplified format, including:
    - [ ] summarize the open-source license types into lay terms
    - [ ] create a compatibility table of license types
    - [ ] create a table of our most commonly used software packages and what licenses they use
- [ ] create a page on the DPMCN wiki explaining how to write a CITATION file
